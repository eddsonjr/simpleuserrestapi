from flask import Flask,request, jsonify
from controler import Controller
import json


app = Flask (__name__)
controller = Controller()


#http codes
SUCCESS = 200
NOT_FOUND = 404
INTERNAL_ERROR = 500
BAD_REQUEST = 400


#Definindo as rotas
#rota principal, apenas mensagem de boas vindas
@app.route('/')
def hello():
	return 'Hello, World. This is SimpleUserApi'


#rota para listar todos os usuarios
@app.route('/allUsers',methods=['GET'])
def listAllUsers():
	result = controller.listAllUsers()
	if(result != None):
		return result 
	else:
		createResponse(INTERNAL_ERROR,'empty data or internal server error')





#rota para adicionar um novo usuario na base de dados
@app.route('/addUser', methods=['POST'])
def addUser():
	contentReceived = request.get_json()
	print(str(contentReceived))
	result = controller.addUser(contentReceived)
	if(result == 1):
		return createResponse(SUCCESS,'user added successfully')
	else: 
		return createResponse(BAD_REQUEST,'cannot add new user')



@app.route("/deleteUser/<id>", methods=["DELETE"])
def deleteUser(id):
	result = controller.deleteUser(id)	
	if(result == 1):
		return createResponse(SUCCESS,'user deleted successfully')
	else:
		return createResponse(NOT_FOUND,'error when delete user')




@app.route("/getUser/<id>", methods=["GET"])
def getUserById(id):
	result = controller.getUserById(id)
	if(result != None):
		return result
	else:
		return createResponse(NOT_FOUND,'user not found')



@app.route("/updateUser", methods=["POST","GET"])
def updateUser():
	contentReceived = request.get_json()
	print(contentReceived)
	result = controller.updateUser(contentReceived)
	if(result == 1):
		return createResponse(SUCCESS,'user updated')
	else:
		return createResponse(INTERNAL_ERROR,'cannot update this user')





#funcao para criar um response 
def createResponse(code,message):
	responseJson = {'code': int(code), 'message': message}
	response = app.response_class(
			response = json.dumps(responseJson),
			status = int(code),
			mimetype = 'application/json'
		)
	return response






if __name__ == '__main__':
	app.run()