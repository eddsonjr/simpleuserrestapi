'''
Este arquivo controla as operacoes de banco de dados
Edson Jr
Dez 2022

'''

import mysql.connector


class Database: 

	def __init__(self):
		#iniciando a conexao com o banco de dados
		self.dbConnection = mysql.connector.connect(
			host='localhost',
			user='edsonjr',
			password='password',
			database='SimpleUserList'
		)

		#cursor do banco de dados
		self.dbCursor = self.dbConnection.cursor(dictionary=True)



	#lista todos os usuarios dentro do banco de dados
	def listAll(self):
		sqlCommand = f'SELECT * FROM USER'
		self.dbCursor.execute(sqlCommand)
		dataFetched = self.dbCursor.fetchall()
		return dataFetched





	#insere um novo usuario na base de dados
	def insertUser(self,user):
		sqlCommand = f'INSERT INTO USER (name,phone,email,nick,imgUrl) VALUES (%s,%s,%s,%s,%s)'
		data=(
			user.name,
			user.phone,
			user.email,
			user.nick,
			user.imgUrl
		)
		self.dbCursor.execute(sqlCommand,data)
		self.dbConnection.commit()
		recordsAffected = self.dbCursor.rowcount
		return recordsAffected



	#remove um usuario da base de dados
	def deleteUser(self,userId):
		sqlCommand = f'DELETE FROM USER WHERE id = {userId}'
		self.dbCursor.execute(sqlCommand)
		self.dbConnection.commit()
		recordsAffected = self.dbCursor.rowcount
		return recordsAffected
	




	#pesquisa um usuario pelo id e retorna, caso encontre
	def getUserById(self,userId):
		sqlCommand = f'SELECT * FROM USER WHERE id = {userId}'
		self.dbCursor.execute(sqlCommand)
		dataFetched = self.dbCursor.fetchone()
		return dataFetched




	#pesquisa usuarios pelo nome 
	def getUsersByName(self,name):
		sqlCommand = f'SELECT * FROM USER WHERE name LIKE \'%{name}%\''
		self.dbCursor.execute(sqlCommand)
		dataFetched = self.dbCursor.fetchall()
		return dataFetched





	#atualiza um usario na base
	def updateUser(self,user):
		sqlCommand = "UPDATE USER SET name = %s,phone = %s,email = %s,nick =  %s,imgUrl = %s WHERE id = %s"
		data = (user.name,user.phone,user.email,user.nick,user.imgUrl,user.id)
		self.dbCursor.execute(sqlCommand,data)
		self.dbConnection.commit()
		recordsAffected = self.dbCursor.rowcount
		return recordsAffected

