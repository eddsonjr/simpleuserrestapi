'''
Este arquivo serve para realizacao de testes
Edson jr
Dez 2022
'''
from database import Database
from user import User

#Testa a chamada de todos os dados do banco de dados
def testShowAllUsers():
	return database.listAll()


#procura usuarios por nome
def searchByName(name):
	dataFetched = database.getUsersByName(name)
	print(dataFetched)





#Testa procurar um usuario por id e retorna-lo
def getUserById(userId):
	dataFetched = database.getUserById(userId)
	if(dataFetched == None):
		print("DATA NOT FOUND!")

	return dataFetched




#atualiza um usuario
def updateUser(user):
	db = Database()
	isUpdated = db.updateUser(user)
	print(str(isUpdated))
	if(isUpdated == 0):
		print("User not found. Nothing to update!")
		return 

	print("User data updated successfully") 




#Testa criar um objeto do tipo User Baseado em dados vindos do banco
def createUserObj(dataFromDB):
	userObj = User(dataFromDB[0],dataFromDB[1],dataFromDB[2],dataFromDB[3],dataFromDB[4])
	return userObj



# print("Database data: \n" + str(testShowAllUsers()))
# print("-------------------------------------------")

# # dataFromDB = testShowAllUsers()
# # userObj = dataFromDB[0]
# # print(userObj)

# print("-------------------------------------------")
# print("Trying retrieve data from user id: \n" + str(getUserById(9)))



newUser = User(
	3,
	'Yang Xiao L.',
	'+559212345678',
	'yang_xiao@cisc.com',
	'yangx',
	'none')


newUser2 = User(
	3,
	'Yang Xiao L.',
	'+559212345678',
	'yang_xiao@cisc.com',
	'yangx',
	None)

updateUser(newUser2)



# searchByName('L')