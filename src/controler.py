'''
 Controler - Responsavel por intermediar o app.py e 
 e o repository. 


 Edson Jr
'''
from database import Database
import json 
from user import User

class Controller:

	__dbgTag = "[Controller]: "

	#inicializa a classe de database no construtor do controller
	def __init__(self):
		self.db = Database()



	def listAllUsers(self):
		print(self.__dbgTag + "Requesting for all users")
		listOfAllUsers = self.db.listAll()
		if(listOfAllUsers != None):
			jsonData = json.dumps(listOfAllUsers)
			print(self.__dbgTag + " resultado: " + str(jsonData))
			return jsonData

		return None




	def getUserById(self,id):
		print(self.__dbgTag + "Trying get user id " + str(id))
		userFound = self.db.getUserById(id)
		if(userFound != None):
			jsonData = json.dumps(userFound)
			print(self.__dbgTag + "User found: " + str(jsonData))
			# user = self.createUserObjFromTuple(userFound)
			return jsonData
		
		return None




	def addUser(self,userJson):
		print(self.__dbgTag + "Trying add new user")

		#adicionando null no campo de id do usuario, pois ele e autoincremento
		userJson.update({"id":None})

		#convertendo o json passado como parametro em um objeto
		jsonDump = json.dumps(userJson)
		loadedJson = json.loads(jsonDump)
		newUserObj = User(**loadedJson)

		#tentando adicionar o novo usuario no banco de dados
		addResultOperation = self.db.insertUser(newUserObj)
		if(addResultOperation == 1):
			print("User added sucessfully")
		else:
			print("Fail to add new user")

		return addResultOperation




	def deleteUser(self,userID):
		print(self.__dbgTag + "Trying delete user  id: " + str(userID))
		deleteUserOperation = self.db.deleteUser(userID)
		if(deleteUserOperation == 1):
			print("User removed sucessfully")
		else:
			print("Fail to remove the user ")

		return deleteUserOperation



	def updateUser(self,userJson):
		print(self.__dbgTag + "Trying update user infos")
		#convertendo o json passado como parametro em um objeto User
		jsonDump = json.dumps(userJson)
		loadedJson = json.loads(jsonDump)
		newUserObj = User(**loadedJson)

		#tentando adicionar o novo usuario no banco de dados
		updateUserResult = self.db.updateUser(newUserObj)
		if(updateUserResult == 1):
			print("User updated sucessfully")
		else:
			print("Fail to update user information")

		return updateUserResult




	#retorna um objeto User criado a partir da tupla que e retornada do banco de dados
	def createUserObjFromTuple(self,tuple):
		userObj = User(
				id = tuple[0],
				name = tuple[1],
				phone = tuple[2],
				email = tuple[3],
				nick = tuple[4],
				imgUrl = tuple[5]
			)
		return userObj


